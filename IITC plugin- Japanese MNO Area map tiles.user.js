// ==UserScript==
// @id          iitc-plugin-Japanese_mno_coverage_layer
// @name        IITC plugin: Japanese MNO Area map tiles
// @category    Layer
// @namespace   https://github.com/jonatkins/ingress-intel-total-conversion
// @description IITC plugin: 日本4キャリ エリアマップ レイヤ
// @author      inok6743, ozero,nighthackz
// @updateURL   https://gitlab.com/NHz/Plugin/raw/master/IITC%20plugin-%20Japanese%20MNO%20Area%20map%20tiles.user.js
// @downloadURL https://gitlab.com/NHz/Plugin/raw/master/IITC%20plugin-%20Japanese%20MNO%20Area%20map%20tiles.user.js
// @include     https://*.ingress.com/*
// @match       https://*.ingress.com/*
// @version     0.0.6.20210217
// @grant       none
// ==/UserScript==

function wrapper(plugin_info) {
    // ensure plugin framework is there, even if iitc is not yet loaded
    if(typeof window.plugin !== 'function') window.plugin = function() {};


    // PLUGIN START ////////////////////////////////////////////////////////

    //オプション値
    var STORAGE_KEY = 'japaneseMnoCoverageLayer-option';
    var OptionData = {};

    var TileLayerDocomo = null;
    var TileLayerSoftBank = null;
    var TileLayerKddiLte = null;
    var TileLayerKddiCdma = null;
    var TileLayerRakuten = null;
    // use own namespace for plugin
    window.plugin.japaneseMnoCoverageLayer = {
        //Docomo
        addDocomoOsmLayer: function(){
            var tileOpt = {
                attribution: 'Docomo Area map @NTT Docomo',
                maxNativeZoom: 14,
                maxZoom: 21,
                minZoom: 9,
                opacity: OptionData.opacity.docomo / 10
            };
            var tileUrl = 'https://servicearea.nttdocomo.co.jp/map/1014/0000000000000000/{z}/{x}/{z}_{x}_{y}.gif';
            TileLayerDocomo = new L.TileLayer(tileUrl, tileOpt);
            window.addLayerGroup('Docomo Area Map', TileLayerDocomo, true);
        },
        //Softbank
        addSoftbankOsmLayer: function(){
            //
            var tileOpt = {
                attribution: 'Softbank Area map @SBM',
                maxNativeZoom: 12,
                maxZoom: 21,
                minZoom: 9,
                opacity: OptionData.opacity.softBank / 10
            };
            var tileUrl = 'https://tiles.areamap.mb.softbank.jp/ServiceAreaMap_Hybrid4GLTE_ACT/20180228000/{z}/{y}/{x}.png';
            TileLayerSoftBank = new L.TileLayer(tileUrl, tileOpt);
            window.addLayerGroup('Softbank Area Map', TileLayerSoftBank, true);
        },
        //KDDI LTE
        addKddilteOsmLayer: function(){
            //
            var tileOpt = {
                attribution: 'au(4G) Area map @KDDI',
                maxNativeZoom: 12,
                maxZoom: 21,
                minZoom: 9,
                opacity: OptionData.opacity.kddiLTE / 10
            };
            var tileUrl = 'http://www13.info-mapping.com/au/map/services/tile.asp?l=1&t=B&x={x}&y={y}&z={z}';
            TileLayerKddiLte = new L.TileLayer(tileUrl, tileOpt);
            window.addLayerGroup('au(4G) Area Map', TileLayerKddiLte, true);
        },

        //KDDI CDMA
        addKddicdmaOsmLayer: function(){
            //
            var tileOpt = {
                attribution: 'au(3G) Area map @KDDI',
                maxNativeZoom: 12,
                maxZoom: 21,
                minZoom: 9,
                opacity: OptionData.opacity.kddiCDMA / 10
            };
            var tileUrl = 'http://www13.info-mapping.com/au/map/services/tile.asp?l=0&t=B&x={x}&y={y}&z={z}';
            TileLayerKddiCdma = new L.TileLayer(tileUrl, tileOpt);
            window.addLayerGroup('au(3G) Area Map', TileLayerKddiCdma, true);
        },
        //Rakuten
        addRakutenOsmLayer: function(){
            //
            var tileOpt = {
                attribution: 'Rakuten Area map @Rakuren',
                maxNativeZoom: 12,
                maxZoom: 21,
                minZoom: 9,
                opacity: OptionData.opacity.rakuten / 10
            };
            var tileUrl = 'https://gateway-api.global.rakuten.com/dsd/geoserver/4g/mno_coverage_map/gwc/service/gmaps?LAYERS=mno_coverage_map:all_map&FORMAT=image/png&TRANSPARENT=TRUE&x={x}&y={y}&zoom={z}';
            TileLayerRakuten = new L.TileLayer(tileUrl, tileOpt);
            window.addLayerGroup('Rakuten Area Map', TileLayerRakuten, true);
        },
    }

    var setup = function(){
        $('#toolbox').append('<a onclick="window.plugin.japaneseMnoCoverageLayer.optionDialog();return false;">4キャリ設定</a>');
        window.plugin.japaneseMnoCoverageLayer.loadOption();

        window.plugin.japaneseMnoCoverageLayer.addDocomoOsmLayer();
        window.plugin.japaneseMnoCoverageLayer.addSoftbankOsmLayer();
        window.plugin.japaneseMnoCoverageLayer.addKddilteOsmLayer();
        window.plugin.japaneseMnoCoverageLayer.addKddicdmaOsmLayer();
        window.plugin.japaneseMnoCoverageLayer.addRakutenOsmLayer();
    }

    // 設定ダイアログ
    window.plugin.japaneseMnoCoverageLayer.optionDialog = function () {
        function localView(_text,_id , _targetLayer) {
            var row = $('<tr></tr>');
            var td1 = $('<td></td>');
            td1.css({
                'vertical-align'     : 'middle'
            });
            td1.text(_text);

            var td2 = $('<td></td>');
            var mnoOpacityView = $('<input>', {
                type : 'range',
                max : 10,
                min : 0,
                id : _id
            });
            td2.append(mnoOpacityView);

            var td3 = $('<td></td>');
            td3.css({
                'vertical-align'     : 'middle'
            });
            var mnoOpacityDisplay = $('<span>' , {
                id : _id + '-display'
            });
            td3.append(mnoOpacityDisplay);

            mnoOpacityView.on('input', function () {
                var value = $(this).val();
                mnoOpacityDisplay.text( value );
                _targetLayer.setOpacity(value / 10);
            });
            row.append(td1,td2,td3);
            return row;
        };

        var table = $('<table>');
        table.append(localView ( 'DOCOMO'    , 'japaneseMnoCoverageLayer-opacity-docomo'   , TileLayerDocomo));
        table.append(localView ( 'SoftBank'  , 'japaneseMnoCoverageLayer-opacity-softbank' , TileLayerSoftBank));
        table.append(localView ( 'KDDI 3G'  , 'japaneseMnoCoverageLayer-opacity-kddilte'  , TileLayerKddiLte));
        table.append(localView ( 'KDDI 4G' , 'japaneseMnoCoverageLayer-opacity-kddicdma' , TileLayerKddiCdma));
        table.append(localView ( 'Rakuten'   , 'japaneseMnoCoverageLayer-opacity-rakuten'  , TileLayerRakuten));
        var html = $('<div>').append(table);

        dialog({
            html: html,
            id: 'japaneseMnoCoverageLayer-options',
            title: '4キャリ設定',
            focusCallback: function() {
                //debugger;
                $('#japaneseMnoCoverageLayer-opacity-docomo').val(OptionData.opacity.docomo);
                $('#japaneseMnoCoverageLayer-opacity-docomo-display').text( OptionData.opacity.docomo );
                $('#japaneseMnoCoverageLayer-opacity-softbank').val(OptionData.opacity.softBank);
                $('#japaneseMnoCoverageLayer-opacity-softbank-display').text( OptionData.opacity.softBank);
                $('#japaneseMnoCoverageLayer-opacity-kddilte').val(OptionData.opacity.kddiLTE);
                $('#japaneseMnoCoverageLayer-opacity-kddilte-display').text( OptionData.opacity.kddiLTE);
                $('#japaneseMnoCoverageLayer-opacity-kddicdma').val(OptionData.opacity.kddiCDMA);
                $('#japaneseMnoCoverageLayer-opacity-kddicdma-display').text( OptionData.opacity.kddiCDMA);
                $('#japaneseMnoCoverageLayer-opacity-rakuten').val(OptionData.opacity.rakuten);
                $('#japaneseMnoCoverageLayer-opacity-rakuten-display').text( OptionData.opacity.rakuten);
            },
            closeCallback:function() {
                OptionData.opacity.docomo = $('#japaneseMnoCoverageLayer-opacity-docomo').val();
                OptionData.opacity.softBank = $('#japaneseMnoCoverageLayer-opacity-softbank').val();
                OptionData.opacity.kddiLTE = $('#japaneseMnoCoverageLayer-opacity-kddilte').val();
                OptionData.opacity.kddiCDMA = $('#japaneseMnoCoverageLayer-opacity-kddicdma').val();
                OptionData.opacity.rakuten = $('#japaneseMnoCoverageLayer-opacity-rakuten').val();
                window.plugin.japaneseMnoCoverageLayer.saveOption();

                TileLayerDocomo.setOpacity(OptionData.opacity.docomo / 10);
                return true;
            }
        });
    }
    //  オプション値をロード
    window.plugin.japaneseMnoCoverageLayer.loadOption = function () {
        var stream = localStorage.getItem(STORAGE_KEY);
        var _data = (stream === null) ? {} : JSON.parse(stream);
        //初期値
        if (!!!_data.opacity) {
            _data.opacity = {
                docomo : '3',
                softBank : '3',
                kddiLTE : '3',
                kddiCDMA : '3',
                rakuten : '10'
            };
        }

        OptionData.opacity = _data.opacity;
    };
    // オプション値を保存
    window.plugin.japaneseMnoCoverageLayer.saveOption = function () {
        var stream = JSON.stringify(OptionData);

        localStorage.setItem(STORAGE_KEY,stream);
    };
    // PLUGIN END //////////////////////////////////////////////////////////


    setup.info = plugin_info; //add the script info data to the function as a property
    if(!window.bootPlugins) window.bootPlugins = [];
    window.bootPlugins.push(setup);
    // if IITC has already booted, immediately run the 'setup' function
    if(window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end
// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);
