// ==UserScript==
// @id             iitc-plugin-overlaymap-gsicyberjapan
// @name           IITC Plugin:[20180612.01]GSI Overlay Map
// @category       Layer
// @version        0.0.4.20180612.01
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @namespace      https://gitlab.com/NHz/Plugin
// @author         nighthackz
// @updateURL      https://gitlab.com/NHz/Plugin/raw/master/IITC%20Plugin-GSI%20Overlay%20Map.user.js
// @downloadURL    https://gitlab.com/NHz/Plugin/raw/master/IITC%20Plugin-GSI%20Overlay%20Map.user.js
// @description    国土地理院のオーバーレイマップ
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==

// use own namespace for plugin
function wrapper(plugin_info) {
    if(typeof window.plugin !== 'function') window.plugin = function() {};

    // オプション値
	var STORAGE_KEY = 'gsiCyberJapanOverlayMapLayer-option';
	var OptionData = {};

    //赤色立体マップ
	var OverlaySekishokuMap = null;
    // 陰影起伏図
    var OverlayHillshadeMap = null;

    window.plugin.gsiCyberJapanOverlayMapLayer = {};
	// 赤色立体マップ追加
 	window.plugin.gsiCyberJapanOverlayMapLayer.addSekishokuMap = function () {
            var sekishokuMapOpt = {
                attribution: 'GSI Sekishoku Map',
                maxNativeZoom: 14,
                maxZoom: 21,
                reuseTiles: true,
                detectRetina : true,
                opacity: OptionData.opacity.sekishoku / 10
            };
            var sekishokuUrl = 'https://cyberjapandata.gsi.go.jp/xyz/sekishoku/{z}/{x}/{y}.png';
            OverlaySekishokuMap = new L.TileLayer(sekishokuUrl, sekishokuMapOpt);
            window.addLayerGroup('GSI Sekishoku Map', OverlaySekishokuMap, false);

            var hillshadeMapOpt = {
                attribution: 'GSI Hillshade Map',
                maxNativeZoom: 16,
                maxZoom: 21,
                reuseTiles: true,
                detectRetina : true,
                opacity: OptionData.opacity.hillshade / 10
            };
            var hillshadeUrl = 'https://cyberjapandata.gsi.go.jp/xyz/hillshademap/{z}/{x}/{y}.png';
            OverlayHillshadeMap = new L.TileLayer(hillshadeUrl, hillshadeMapOpt);
            window.addLayerGroup('GSI Hillshade Map', OverlayHillshadeMap, false);
	}
    // 設定ダイアログ
    window.plugin.gsiCyberJapanOverlayMapLayer.optionDialog = function () {
        function localView(_text,_id , _targetLayer) {
            var row = $('<tr></tr>');
            var td1 = $('<td></td>');
            td1.css({
                'vertical-align'     : 'middle'
            });
            td1.text(_text);

            var td2 = $('<td></td>');
            var mnoOpacityView = $('<input>', {
                type : 'range',
                max : 10,
                min : 0,
                id : _id
            });
            td2.append(mnoOpacityView);

            var td3 = $('<td></td>');
            td3.css({
                'vertical-align'     : 'middle'
            });
            var mnoOpacityDisplay = $('<span>' , {
                id : _id + '-display'
            });
            td3.append(mnoOpacityDisplay);

            mnoOpacityView.on('input', function () {
                var value = $(this).val();
                mnoOpacityDisplay.text( value );
                _targetLayer.setOpacity(value / 10);
            });
            row.append(td1,td2,td3);
            return row;
        };

        var table = $('<table>');
        table.append(localView ( '赤色立体図','gsi-cyber-japan-overlay-map-layer-opacity-sekishoku' , OverlaySekishokuMap));
        table.append(localView ( '陰影起伏図','gsi-cyber-japan-overlay-map-layer-opacity-hillshade' , OverlayHillshadeMap));
        var html = $('<div>').append(table);

        dialog({
            html: html,
            id: 'gsiCyberJapanOverlayMapLayer-options',
            title: 'GSI設定',
            focusCallback: function() {
                $('#gsi-cyber-japan-overlay-map-layer-opacity-sekishoku').val(OptionData.opacity.sekishoku);
                $('#gsi-cyber-japan-overlay-map-layer-opacity-sekishoku-display').text( OptionData.opacity.sekishoku );
                $('#gsi-cyber-japan-overlay-map-layer-opacity-hillshade').val(OptionData.opacity.hillshade);
                $('#gsi-cyber-japan-overlay-map-layer-opacity-hillshade-display').text( OptionData.opacity.hillshade );
            },
            closeCallback:function() {
                OptionData.opacity.sekishoku = $('#gsi-cyber-japan-overlay-map-layer-opacity-sekishoku').val();
                OptionData.opacity.hillshade = $('#gsi-cyber-japan-overlay-map-layer-opacity-hillshade').val();
                window.plugin.japaneseMnoCoverageLayer.saveOption();
                return true;
            }
        });
    }
    //  オプション値をロード
    window.plugin.gsiCyberJapanOverlayMapLayer.loadOption = function () {
        var stream = localStorage.getItem(STORAGE_KEY);
        var _data = (stream === null) ? {} : JSON.parse(stream);
        //初期値
        if (!!!_data.opacity) {
            _data.opacity = {
                sekishoku : '4',
                hillshade : '4'
            };
        }

        OptionData.opacity = _data.opacity;
    };
    // オプション値を保存
    window.plugin.gsiCyberJapanOverlayMapLayer.saveOption = function () {
        var stream = JSON.stringify(OptionData);
        localStorage.setItem(STORAGE_KEY,stream);
    };

	//セットアップ
    var setup = function(){
        window.plugin.gsiCyberJapanOverlayMapLayer.loadOption();
		window.plugin.gsiCyberJapanOverlayMapLayer.addSekishokuMap();
        $('#toolbox').append('<a onclick="window.plugin.gsiCyberJapanOverlayMapLayer.optionDialog();return false;">GSI設定</a>');
    }

    setup.info = plugin_info; //add the script info data to the function as a property
    if(!window.bootPlugins) window.bootPlugins = [];
    window.bootPlugins.push(setup);
    // if IITC has already booted, immediately run the 'setup' function
    if(window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end
// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);

